class ImagesController < ApplicationController
  
  def index
    @images = Image.all.order("created_at DESC")
  end
  
  def new
    @image = Image.new
  end
  def show
    @image = Image.find(params[:id])
  end
  def create
    @img = Image.new(image_params)
    
    if @img.save
      redirect_to Image.last
    else
      render 'new'
    end
  end
  
  private
    def image_params
      params.require(:image).permit(:title, :description, :author, :img)
    end
end
